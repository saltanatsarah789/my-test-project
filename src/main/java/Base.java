import io.github.bonigarcia.wdm.ChromeDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import java.util.concurrent.TimeUnit;

public class Base {


    WebDriver driver;
    @BeforeMethod
    public void setUp(){
        ChromeDriverManager.getInstance().setup();
        driver=new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
        String url="https://www.google.com/";
        driver.navigate().to(url);
    }

    @AfterMethod
    public void after(){
        driver.close();
    }


}
